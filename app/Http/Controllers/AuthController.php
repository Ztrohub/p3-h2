<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist(){
        return view('register');
    }

    // public function welcome(){
    //     return view('welcome');
    // }

    public function welcome_post(Request $request){
        $name = $request['fname'] . " " . $request['lname'];
        return view('welcome', ['nama' => $name]);
    }
}
