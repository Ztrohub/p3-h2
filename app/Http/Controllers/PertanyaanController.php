<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('CRUD.pertanyaan.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());

        // Untuk validasi inputan
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required',
        ]);
        
        // // Store input dengan QUERY BUILDER
        // $query = DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi'],
        // ]);
        
        // // Store input dengan MODEL metode SAVE
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->isi = $request['isi'];
        // $pertanyaan->save();

        // Store input dengan MODEL metode MASS ASSIGNMENT
        $pertanyaan = Pertanyaan::create([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);

        return redirect('/pertanyaan')->with(
            'disimpan',
            'Pertanyaan Berhasil Disimpan!'
        );
    }

    public function index()
    {
        // // Index dengan QUERY BUILDER
        // $pertanyaan = DB::table('pertanyaan')->get(); // select * from pertanyaan

        // Index dengan MODEL
        $pertanyaan = Pertanyaan::all();

        // dd($pertanyaan);
        return view('CRUD.pertanyaan.index', compact('pertanyaan'));
    }

    public function show($pertanyaan_id)
    {
        // // Mencari Data dengan QUERY BUILDER
        // $pertanyaan = DB::table('pertanyaan')
        //     ->where('id', $pertanyaan_id)
        //     ->first();

        // Mencari Data dengan MODEL
        $pertanyaan = Pertanyaan::find($pertanyaan_id);

        // dd($pertanyaan);
        return view('CRUD.pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($pertanyaan_id)
    {
        // // Mencari Data dengan QUERY BUILDER
        // $pertanyaan = DB::table('pertanyaan')
        //     ->where('id', $pertanyaan_id)
        //     ->first();

        // Mencari Data dengan MODEL
        $pertanyaan = Pertanyaan::find($pertanyaan_id);

        return view('CRUD.pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($pertanyaan_id, Request $request)
    {
        // dd($request->all());

        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required',
            ]);
            
        // // Update dengan QUERY BUILDER

        // $query = DB::table('pertanyaan')
        //     ->where('id', $pertanyaan_id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi'],
        //     ]);

        // Update dengan MODEL pake MASS UPDATE
        $update = Pertanyaan::find($pertanyaan_id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);

        return redirect('/pertanyaan')->with(
            'diubah',
            'Pertanyaan Berhasil Diubah!'
        );
    }

    public function destroy($pertanyaan_id)
    {
        // // Delete dengan QUERY BUILDER
        // $query = DB::table('pertanyaan')
        //     ->where('id', $pertanyaan_id)
        //     ->delete();

        // Delete dengan MODEL
        Pertanyaan::destroy($pertanyaan_id);

        return redirect('/pertanyaan')->with(
            'dihapus',
            'Pertanyaan Berhasil Dihapus!'
        );
    }
}
