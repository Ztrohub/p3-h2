<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * php artisan make:model Nama
 * 
 * Keuntungan model adalah:
 *      1. Lebih cepat (tidak ribet)
 *      2. Dapat memasukkan timestamp() otomatis
 * 
 * Model dibuat dengan nama singular diawali huruf besar
 * contoh: User
 * 
 * Model secara otomatis mencari nama table yang sama plural
 * contoh: users
 * 
 * Jika nama table tidak plural atau bahasa indonesia
 * gunakan (protected $table = 'nama_database')
 * 
 * Model menggunakan nama kolom 'id' sebagai primary key
 * Jika db tidak menggunakan nama berbeda gunakan
 * (protected $primaryKey = 'nama_id')
 */

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';

    // Untuk metode MASS ASSIGNMENT

    // sebagai whitelist
    // protected $fillable = ['judul', 'isi'];

    // sebagai blacklist
    protected $guarded = []; //sebagai blacklist

}
