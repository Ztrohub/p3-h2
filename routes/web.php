<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// P3 H2
// Route::get('/', 'HomeController@index');

// Route::get('/register', 'AuthController@regist');

// // Route::get('/welcome', 'AuthController@welcome');

// Route::post('/welcome', 'AuthController@welcome_post');

// Route::get('/master', function (){
//     return view('adminlte.master');
// });

// Route::get('/index', function (){
//     return view('items.index');
// });

// P4 H3
// Route::get('/', 'HomeController@index');

// // Route::get('/data-tables', 'HomeController@tables');

// Route::get('/pertanyaan/create', 'PertanyaanController@create');

// Route::post('/pertanyaan', 'PertanyaanController@store');

// Route::get('/pertanyaan', 'PertanyaanController@index');

// Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');

// Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');

// Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');

// Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

// Route::resource('question', 'QuestionController');

Route::resource('pertanyaan', 'PertanyaanController');
