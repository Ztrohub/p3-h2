<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label><br /><br />
        <input type="text" id="fname" name="fname" /><br /><br />

        <label for="lname">Last name:</label><br /><br />
        <input type="text" id="lname" name="lname" /><br /><br />

        <label for="gender">Gender: </label><br /><br />
        <input type="radio" id="male" name="gender" value="male" />
        <label for="male">Male</label><br />
        <input type="radio" id="female" name="gender" value="female" />
        <label for="female">Female</label><br />
        <input type="radio" id="other" name="gender" value="other" />
        <label for="other">Other</label>
        <br /><br />

        <label for="nationality">Nationality:</label><br /><br />

        <select name="nationality" id="nationality">
            <option value="indonesian" selected>Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
        <br><br>

        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" id="indo" name="indo" value="Bike" />
        <label for="indo">Bahasa Indonesia</label><br />
        <input type="checkbox" id="english" name="english" value="Car" />
        <label for="english"> English</label><br />
        <input type="checkbox" id="other" name="other" value="Boat" />
        <label for="other"> Other</label>
        <br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea id="bio" name="bio" rows="10" cols="25"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>