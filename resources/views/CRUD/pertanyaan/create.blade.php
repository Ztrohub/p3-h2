@extends('adminlte.master')

@section('content')
<div class="m-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->

        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" name="judul" class="form-control" id="judul" value="{{ old('judul','') }}"
                        placeholder="Saya ingin bertanya tentang...">
                </div>
                @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="isi">Deskripsi Pertanyaan</label>
                    <textarea id="isi" name="isi" class="form-control" placeholder="Jelaskan pertanyaan anda" rows="4"
                        col="50">{{ old('isi', '')}}</textarea>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tanyakan!</button>
            </div>
        </form>
    </div>
</div>
@endsection