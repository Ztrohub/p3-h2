@extends('adminlte.master')

@section('content')
<div class="m-3">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if(session('disimpan'))
      <div class="alert alert-success" role="alert">
        {{ session('disimpan') }}
      </div>
      @endif
      @if(session('diubah'))
      <div class="alert alert-warning" role="alert">
        {{ session('diubah') }}
      </div>
      @endif
      @if(session('dihapus'))
      <div class="alert alert-danger" role="alert">
        {{ session('dihapus') }}
      </div>
      @endif
      <a class="btn btn-primary mb-3" href="{{ route('pertanyaan.create') }}">Buat Pertanyaan Baru</a>
      <table class="table table-bordered">
        <thead>
          <tr class="text-center">
            <th style="width: 2%">#</th>
            <th style="width: 30%">Judul</th>
            <th>Deskripsi</th>
            <th style="width: 15%">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($pertanyaan as $key => $item)
          <tr>
            <td>{{ $key +1 }}.</td>
            <td>{{ $item -> judul}}</td>
            <td>{{ $item -> isi}}</td>
            <td>
              <div class="d-flex">
                <a class="btn btn-info btn-sm d-flex justify-content-center" href="/pertanyaan/{{$item->id}}">Show</a>
                <a class="btn btn-warning btn-sm d-flex justify-content-center mx-1"
                  href="/pertanyaan/{{$item->id}}/edit">Edit</a>
                <form action="/pertanyaan/{{$item->id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" class="btn btn-danger btn-sm d-flex justify-content-center" value="Delete">
                </form>
              </div>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="4" class="text-center">Tidak ada pertanyaan!</td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    {{-- <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div> --}}
  </div>
</div>
@endsection