@extends('adminlte.master')

@section('content')
<div class="m-3">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">{{$pertanyaan -> judul}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <p class="card-text">{{$pertanyaan -> isi}}</p>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
      <a class="btn btn-info btn-sm" href="/pertanyaan">Kembali</a>
    </div>
  </div>
  </div>
  @endsection